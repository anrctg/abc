<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ANIK</title>
    <style>
        .main {
            width: 800px;
        }

        .sub {
            width: 70px;
            display: inline-block;
        }
        .sub4 {
            display: inline-block;
            margin-left: -39px;
        }
        .main .sub:nth-child(2){
            margin-left: -14px;
        }
    </style>
</head>
<body>
<div class="main">
    <div class="sub"><?php include "a.php" ;?></div>
    <div class="sub"><?php include "n.php" ;?></div>
    <div class="sub"><?php include "i.php"; ?></div>
    <div class="sub4"><?php include "k.php"; ?></div>
</div>

</body>
</html>